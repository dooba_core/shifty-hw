<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="Refs" color="11" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="dooba">
<packages>
<package name="BSS138">
<wire x1="1.45" y1="0.7" x2="1.45" y2="-0.7" width="0.127" layer="21"/>
<wire x1="1.45" y1="-0.7" x2="-1.45" y2="-0.7" width="0.127" layer="21"/>
<wire x1="-1.45" y1="-0.7" x2="-1.45" y2="0.7" width="0.127" layer="21"/>
<wire x1="-1.45" y1="0.7" x2="1.45" y2="0.7" width="0.127" layer="21"/>
<smd name="D" x="0" y="1.1" dx="1.4" dy="1" layer="1" rot="R270"/>
<smd name="S" x="0.95" y="-1.1" dx="1.4" dy="1" layer="1" rot="R270"/>
<smd name="G" x="-0.95" y="-1.1" dx="1.4" dy="1" layer="1" rot="R270"/>
<text x="-0.95" y="0" size="0.4064" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-0.95" y="-0.475" size="0.4064" layer="27" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="0805">
<wire x1="-0.508" y1="0.635" x2="0.508" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.508" y1="0.635" x2="0.508" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.508" y1="-0.635" x2="-0.508" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-0.635" x2="-0.508" y2="0.635" width="0.127" layer="21"/>
<smd name="1" x="-1" y="0" dx="0.8" dy="1.4" layer="1"/>
<smd name="2" x="1" y="0" dx="0.8" dy="1.4" layer="1"/>
<text x="-0.2" y="-0.5" size="0.254" layer="25" rot="R90">&gt;Name</text>
<text x="0.2" y="-0.5" size="0.254" layer="27" rot="R90">&gt;Value</text>
<polygon width="0.127" layer="21">
<vertex x="-0.127" y="-0.635"/>
<vertex x="0.508" y="0"/>
<vertex x="0.508" y="-0.635"/>
</polygon>
</package>
<package name="0402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<wire x1="-1.4732" y1="0.4826" x2="1.1176" y2="0.4826" width="0.0508" layer="117"/>
<wire x1="1.1176" y1="0.4826" x2="1.4732" y2="0.4826" width="0.0508" layer="117"/>
<wire x1="1.4732" y1="0.4826" x2="1.4732" y2="-0.4826" width="0.0508" layer="117"/>
<wire x1="1.4732" y1="-0.4826" x2="1.1176" y2="-0.4826" width="0.0508" layer="117"/>
<wire x1="1.1176" y1="-0.4826" x2="-1.4732" y2="-0.4826" width="0.0508" layer="117"/>
<wire x1="-1.4732" y1="-0.4826" x2="-1.4732" y2="0.4826" width="0.0508" layer="117"/>
<text x="-0.4953" y="-0.3048" size="0.6096" layer="117" ratio="15">&gt;Name</text>
<wire x1="1.1176" y1="0.4826" x2="1.1176" y2="-0.4826" width="0.0508" layer="117"/>
</package>
<package name="RES8MM">
<pad name="P$1" x="-4" y="0" drill="0.8" diameter="1.4224"/>
<pad name="P$2" x="4" y="0" drill="0.8" diameter="1.4224"/>
<wire x1="-2.794" y1="0" x2="-1.778" y2="0" width="0.127" layer="21"/>
<wire x1="-1.778" y1="0" x2="-1.778" y2="0.762" width="0.127" layer="21"/>
<wire x1="-1.778" y1="0.762" x2="1.778" y2="0.762" width="0.127" layer="21"/>
<wire x1="1.778" y1="0.762" x2="1.778" y2="0" width="0.127" layer="21"/>
<wire x1="1.778" y1="0" x2="2.794" y2="0" width="0.127" layer="21"/>
<wire x1="-1.778" y1="0" x2="-1.778" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-1.778" y1="-0.762" x2="1.778" y2="-0.762" width="0.127" layer="21"/>
<wire x1="1.778" y1="-0.762" x2="1.778" y2="0" width="0.127" layer="21"/>
<text x="-2.794" y="1.016" size="0.8128" layer="25">&gt;Name</text>
<text x="-2.794" y="-1.778" size="0.8128" layer="27">&gt;Value</text>
</package>
<package name="RES_STRAIGHT">
<pad name="P$1" x="-1.27" y="0" drill="0.8" diameter="1.4224"/>
<pad name="P$2" x="1.27" y="0" drill="0.8" diameter="1.4224"/>
<wire x1="-2.54" y1="-0.508" x2="-2.54" y2="0.508" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0.508" x2="-1.778" y2="1.27" width="0.127" layer="21"/>
<wire x1="-1.778" y1="1.27" x2="1.778" y2="1.27" width="0.127" layer="21"/>
<wire x1="1.778" y1="1.27" x2="2.54" y2="0.508" width="0.127" layer="21"/>
<wire x1="2.54" y1="0.508" x2="2.54" y2="-0.508" width="0.127" layer="21"/>
<wire x1="2.54" y1="-0.508" x2="1.778" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.778" y1="-1.27" x2="-1.778" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.778" y1="-1.27" x2="-2.54" y2="-0.508" width="0.127" layer="21"/>
<text x="-2.032" y="1.524" size="0.8128" layer="25">&gt;Name</text>
<text x="-2.032" y="-2.286" size="0.8128" layer="27">&gt;Value</text>
<wire x1="0" y1="0.762" x2="0" y2="0.508" width="0.127" layer="27"/>
<wire x1="0" y1="0.508" x2="0.254" y2="0.508" width="0.127" layer="27"/>
<wire x1="0.254" y1="0.508" x2="0.254" y2="-0.508" width="0.127" layer="27"/>
<wire x1="0.254" y1="-0.508" x2="0" y2="-0.508" width="0.127" layer="27"/>
<wire x1="0" y1="-0.508" x2="-0.254" y2="-0.508" width="0.127" layer="27"/>
<wire x1="-0.254" y1="-0.508" x2="-0.254" y2="0.508" width="0.127" layer="27"/>
<wire x1="-0.254" y1="0.508" x2="0" y2="0.508" width="0.127" layer="27"/>
<wire x1="0" y1="-0.508" x2="0" y2="-0.762" width="0.127" layer="27"/>
<wire x1="0" y1="-0.762" x2="0.254" y2="-1.016" width="0.127" layer="27"/>
<wire x1="0" y1="0.762" x2="-0.254" y2="1.016" width="0.127" layer="27"/>
</package>
<package name="PIN_6">
<pad name="1" x="-6.35" y="0" drill="0.8" diameter="1.4224"/>
<pad name="2" x="-3.81" y="0" drill="0.8" diameter="1.4224"/>
<wire x1="-7.62" y1="-0.508" x2="-6.858" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-6.858" y1="-1.27" x2="-5.842" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-5.842" y1="-1.27" x2="-5.334" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-5.334" y1="-0.762" x2="-4.826" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-4.826" y1="-0.762" x2="-4.318" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-4.318" y1="-1.27" x2="-3.302" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-3.302" y1="1.27" x2="-4.318" y2="1.27" width="0.127" layer="21"/>
<wire x1="-4.318" y1="1.27" x2="-4.826" y2="0.762" width="0.127" layer="21"/>
<wire x1="-4.826" y1="0.762" x2="-5.334" y2="0.762" width="0.127" layer="21"/>
<wire x1="-5.334" y1="0.762" x2="-5.842" y2="1.27" width="0.127" layer="21"/>
<wire x1="-5.842" y1="1.27" x2="-6.858" y2="1.27" width="0.127" layer="21"/>
<wire x1="-6.858" y1="1.27" x2="-7.62" y2="0.508" width="0.127" layer="21"/>
<wire x1="-7.62" y1="0.508" x2="-7.62" y2="-0.508" width="0.127" layer="21"/>
<text x="-7.62" y="2.54" size="1.27" layer="25">&gt;Name</text>
<text x="-7.62" y="-3.81" size="1.27" layer="27">&gt;Value</text>
<wire x1="-7.62" y1="1.27" x2="-7.62" y2="0.889" width="0.127" layer="21"/>
<wire x1="-7.62" y1="0.889" x2="-7.239" y2="1.27" width="0.127" layer="21"/>
<wire x1="-7.239" y1="1.27" x2="-7.62" y2="1.27" width="0.127" layer="21"/>
<pad name="3" x="-1.27" y="0" drill="0.8" diameter="1.4224"/>
<wire x1="-4.318" y1="-1.27" x2="-3.302" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-3.302" y1="-1.27" x2="-2.794" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-2.794" y1="-0.762" x2="-2.286" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-2.286" y1="-0.762" x2="-1.778" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.778" y1="-1.27" x2="-0.762" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-0.762" y1="1.27" x2="-1.778" y2="1.27" width="0.127" layer="21"/>
<wire x1="-1.778" y1="1.27" x2="-2.286" y2="0.762" width="0.127" layer="21"/>
<wire x1="-2.286" y1="0.762" x2="-2.794" y2="0.762" width="0.127" layer="21"/>
<wire x1="-2.794" y1="0.762" x2="-3.302" y2="1.27" width="0.127" layer="21"/>
<wire x1="-3.302" y1="1.27" x2="-4.318" y2="1.27" width="0.127" layer="21"/>
<wire x1="-1.778" y1="-1.27" x2="-0.762" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-0.762" y1="1.27" x2="-1.778" y2="1.27" width="0.127" layer="21"/>
<pad name="4" x="1.27" y="0" drill="0.8" diameter="1.4224"/>
<wire x1="-1.778" y1="-1.27" x2="-0.762" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-0.762" y1="-1.27" x2="-0.254" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-0.762" x2="0.254" y2="-0.762" width="0.127" layer="21"/>
<wire x1="0.254" y1="-0.762" x2="0.762" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0.762" y1="-1.27" x2="1.778" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.778" y1="1.27" x2="0.762" y2="1.27" width="0.127" layer="21"/>
<wire x1="0.762" y1="1.27" x2="0.254" y2="0.762" width="0.127" layer="21"/>
<wire x1="0.254" y1="0.762" x2="-0.254" y2="0.762" width="0.127" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.762" y2="1.27" width="0.127" layer="21"/>
<wire x1="-0.762" y1="1.27" x2="-1.778" y2="1.27" width="0.127" layer="21"/>
<wire x1="0.762" y1="-1.27" x2="1.778" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.778" y1="1.27" x2="0.762" y2="1.27" width="0.127" layer="21"/>
<wire x1="0.762" y1="-1.27" x2="1.778" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.778" y1="1.27" x2="0.762" y2="1.27" width="0.127" layer="21"/>
<pad name="5" x="3.81" y="0" drill="0.8" diameter="1.4224"/>
<wire x1="0.762" y1="-1.27" x2="1.778" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.778" y1="-1.27" x2="2.286" y2="-0.762" width="0.127" layer="21"/>
<wire x1="2.286" y1="-0.762" x2="2.794" y2="-0.762" width="0.127" layer="21"/>
<wire x1="2.794" y1="-0.762" x2="3.302" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.302" y1="-1.27" x2="4.318" y2="-1.27" width="0.127" layer="21"/>
<wire x1="4.318" y1="1.27" x2="3.302" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.302" y1="1.27" x2="2.794" y2="0.762" width="0.127" layer="21"/>
<wire x1="2.794" y1="0.762" x2="2.286" y2="0.762" width="0.127" layer="21"/>
<wire x1="2.286" y1="0.762" x2="1.778" y2="1.27" width="0.127" layer="21"/>
<wire x1="1.778" y1="1.27" x2="0.762" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.302" y1="-1.27" x2="4.318" y2="-1.27" width="0.127" layer="21"/>
<wire x1="4.318" y1="1.27" x2="3.302" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.302" y1="-1.27" x2="4.318" y2="-1.27" width="0.127" layer="21"/>
<wire x1="4.318" y1="1.27" x2="3.302" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.302" y1="-1.27" x2="4.318" y2="-1.27" width="0.127" layer="21"/>
<wire x1="4.318" y1="1.27" x2="3.302" y2="1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="0.508" x2="7.62" y2="-0.508" width="0.127" layer="21"/>
<pad name="6" x="6.35" y="0" drill="0.8" diameter="1.4224"/>
<wire x1="3.302" y1="-1.27" x2="4.318" y2="-1.27" width="0.127" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.826" y2="-0.762" width="0.127" layer="21"/>
<wire x1="4.826" y1="-0.762" x2="5.334" y2="-0.762" width="0.127" layer="21"/>
<wire x1="5.334" y1="-0.762" x2="5.842" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.842" y1="-1.27" x2="6.858" y2="-1.27" width="0.127" layer="21"/>
<wire x1="6.858" y1="1.27" x2="5.842" y2="1.27" width="0.127" layer="21"/>
<wire x1="5.842" y1="1.27" x2="5.334" y2="0.762" width="0.127" layer="21"/>
<wire x1="5.334" y1="0.762" x2="4.826" y2="0.762" width="0.127" layer="21"/>
<wire x1="4.826" y1="0.762" x2="4.318" y2="1.27" width="0.127" layer="21"/>
<wire x1="4.318" y1="1.27" x2="3.302" y2="1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="0.508" x2="6.858" y2="1.27" width="0.127" layer="21"/>
<wire x1="6.858" y1="-1.27" x2="7.62" y2="-0.508" width="0.127" layer="21"/>
</package>
<package name="PIN_3X2">
<pad name="1" x="-2.54" y="1.27" drill="0.8" diameter="1.4224"/>
<pad name="2" x="0" y="1.27" drill="0.8" diameter="1.4224"/>
<wire x1="-3.048" y1="-2.54" x2="-2.032" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.524" y1="-2.032" x2="-1.016" y2="-2.032" width="0.127" layer="21"/>
<wire x1="-0.508" y1="2.54" x2="-1.016" y2="2.032" width="0.127" layer="21"/>
<wire x1="-1.016" y1="2.032" x2="-1.524" y2="2.032" width="0.127" layer="21"/>
<wire x1="-1.524" y1="2.032" x2="-2.032" y2="2.54" width="0.127" layer="21"/>
<wire x1="-2.032" y1="2.54" x2="-3.048" y2="2.54" width="0.127" layer="21"/>
<wire x1="-3.048" y1="2.54" x2="-3.81" y2="1.778" width="0.127" layer="21"/>
<wire x1="-3.81" y1="1.778" x2="-3.81" y2="0.762" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.778" x2="3.81" y2="0.762" width="0.127" layer="21"/>
<text x="-3.81" y="3.81" size="1.27" layer="25">&gt;Name</text>
<text x="-3.81" y="-5.08" size="1.27" layer="27">&gt;Value</text>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="2.159" width="0.127" layer="21"/>
<wire x1="-3.81" y1="2.159" x2="-3.429" y2="2.54" width="0.127" layer="21"/>
<wire x1="-3.429" y1="2.54" x2="-3.81" y2="2.54" width="0.127" layer="21"/>
<pad name="3" x="2.54" y="1.27" drill="0.8" diameter="1.4224"/>
<wire x1="-0.508" y1="-2.54" x2="0.508" y2="-2.54" width="0.127" layer="21"/>
<wire x1="1.016" y1="-2.032" x2="1.524" y2="-2.032" width="0.127" layer="21"/>
<wire x1="2.032" y1="-2.54" x2="3.048" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.048" y1="2.54" x2="2.032" y2="2.54" width="0.127" layer="21"/>
<wire x1="2.032" y1="2.54" x2="1.524" y2="2.032" width="0.127" layer="21"/>
<wire x1="1.524" y1="2.032" x2="1.016" y2="2.032" width="0.127" layer="21"/>
<wire x1="1.016" y1="2.032" x2="0.508" y2="2.54" width="0.127" layer="21"/>
<wire x1="0.508" y1="2.54" x2="-0.508" y2="2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.778" x2="3.048" y2="2.54" width="0.127" layer="21"/>
<pad name="4" x="-2.54" y="-1.27" drill="0.8" diameter="1.4224"/>
<pad name="5" x="0" y="-1.27" drill="0.8" diameter="1.4224"/>
<pad name="6" x="2.54" y="-1.27" drill="0.8" diameter="1.4224"/>
<wire x1="-3.81" y1="-1.778" x2="-3.048" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.048" y1="-2.54" x2="3.81" y2="-1.778" width="0.127" layer="21"/>
<wire x1="3.81" y1="-0.762" x2="3.81" y2="-1.778" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-0.762" x2="-3.81" y2="-1.778" width="0.127" layer="21"/>
<wire x1="-3.302" y1="-0.254" x2="-3.81" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-3.302" y1="0.254" x2="-3.302" y2="-0.254" width="0.127" layer="21"/>
<wire x1="-3.81" y1="0.762" x2="-3.302" y2="0.254" width="0.127" layer="21"/>
<wire x1="3.81" y1="-0.762" x2="3.302" y2="-0.254" width="0.127" layer="21"/>
<wire x1="3.302" y1="-0.254" x2="3.302" y2="0.254" width="0.127" layer="21"/>
<wire x1="3.302" y1="0.254" x2="3.81" y2="0.762" width="0.127" layer="21"/>
<wire x1="-1.016" y1="-2.032" x2="-0.508" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-2.54" x2="-1.524" y2="-2.032" width="0.127" layer="21"/>
<wire x1="0.508" y1="-2.54" x2="1.016" y2="-2.032" width="0.127" layer="21"/>
<wire x1="1.524" y1="-2.032" x2="2.032" y2="-2.54" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="BSS138">
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="10.16" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="-5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="-7.62" x2="2.54" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="2.54" y2="12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="2.54" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="7.62" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="10.16" y2="0" width="0.254" layer="94"/>
<wire x1="10.16" y1="0" x2="7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="2.54" x2="10.16" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="-7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="2.54" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="7.62" x2="-7.62" y2="0" width="0.254" layer="94"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="0" x2="-12.7" y2="0" width="0.254" layer="94"/>
<circle x="0" y="0" radius="14.368409375" width="0.254" layer="94"/>
<pin name="D" x="2.54" y="17.78" length="middle" rot="R270"/>
<pin name="S" x="2.54" y="-17.78" length="middle" rot="R90"/>
<pin name="G" x="-17.78" y="0" length="middle"/>
<text x="17.78" y="5.08" size="1.778" layer="95">&gt;Name</text>
<text x="17.78" y="0" size="1.778" layer="96">&gt;Value</text>
<text x="5.08" y="15.24" size="1.778" layer="96">D</text>
<text x="-17.78" y="2.54" size="1.778" layer="96">G</text>
<text x="-2.54" y="-17.78" size="1.778" layer="96">S</text>
</symbol>
<symbol name="RES">
<wire x1="-5.08" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<pin name="1" x="-10.16" y="0" length="middle"/>
<pin name="2" x="10.16" y="0" length="middle" rot="R180"/>
<text x="-5.08" y="2.54" size="1.6764" layer="95">&gt;Name</text>
<text x="-5.08" y="-5.08" size="1.6764" layer="96">&gt;Value</text>
</symbol>
<symbol name="PIN_6">
<pin name="1" x="-7.62" y="0" length="middle"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<text x="-2.54" y="7.62" size="1.27" layer="95" ratio="15">&gt;Name</text>
<text x="-2.54" y="5.08" size="1.27" layer="96" ratio="15">&gt;Value</text>
<pin name="2" x="-7.62" y="-2.54" length="middle"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="2.54" y2="0" width="0.254" layer="94"/>
<pin name="3" x="-7.62" y="-5.08" length="middle"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="-7.62" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="-7.62" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="-7.62" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<pin name="4" x="-7.62" y="-7.62" length="middle"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<pin name="5" x="-7.62" y="-10.16" length="middle"/>
<wire x1="-2.54" y1="-7.62" x2="-2.54" y2="-12.7" width="0.254" layer="94"/>
<wire x1="2.54" y1="-12.7" x2="2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-2.54" y2="-12.7" width="0.254" layer="94"/>
<wire x1="2.54" y1="-12.7" x2="2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-2.54" y2="-12.7" width="0.254" layer="94"/>
<wire x1="2.54" y1="-12.7" x2="2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-2.54" y2="-12.7" width="0.254" layer="94"/>
<wire x1="2.54" y1="-12.7" x2="2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-2.54" y2="-12.7" width="0.254" layer="94"/>
<wire x1="2.54" y1="-12.7" x2="2.54" y2="-7.62" width="0.254" layer="94"/>
<pin name="6" x="-7.62" y="-12.7" length="middle"/>
<wire x1="-2.54" y1="-10.16" x2="-2.54" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-15.24" x2="2.54" y2="-15.24" width="0.254" layer="94"/>
<wire x1="2.54" y1="-15.24" x2="2.54" y2="-10.16" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BSS138" prefix="Q">
<gates>
<gate name="Q1" symbol="BSS138" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BSS138">
<connects>
<connect gate="Q1" pin="D" pad="D"/>
<connect gate="Q1" pin="G" pad="G"/>
<connect gate="Q1" pin="S" pad="S"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RES" prefix="R">
<gates>
<gate name="R1" symbol="RES" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="0805">
<connects>
<connect gate="R1" pin="1" pad="1"/>
<connect gate="R1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="R1" pin="1" pad="1"/>
<connect gate="R1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="8MM" package="RES8MM">
<connects>
<connect gate="R1" pin="1" pad="P$1"/>
<connect gate="R1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="STRAIGHT" package="RES_STRAIGHT">
<connects>
<connect gate="R1" pin="1" pad="P$1"/>
<connect gate="R1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PIN_6" prefix="P">
<gates>
<gate name="P1" symbol="PIN_6" x="0" y="0"/>
</gates>
<devices>
<device name="1LINE" package="PIN_6">
<connects>
<connect gate="P1" pin="1" pad="1"/>
<connect gate="P1" pin="2" pad="2"/>
<connect gate="P1" pin="3" pad="3"/>
<connect gate="P1" pin="4" pad="4"/>
<connect gate="P1" pin="5" pad="5"/>
<connect gate="P1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2LINE" package="PIN_3X2">
<connects>
<connect gate="P1" pin="1" pad="1"/>
<connect gate="P1" pin="2" pad="2"/>
<connect gate="P1" pin="3" pad="3"/>
<connect gate="P1" pin="4" pad="4"/>
<connect gate="P1" pin="5" pad="5"/>
<connect gate="P1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A3P-LOC" urn="urn:adsk.eagle:symbol:13887/1" library_version="1">
<wire x1="256.54" y1="24.13" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="3.81" width="0.1016" layer="94"/>
<wire x1="256.54" y1="3.81" x2="246.38" y2="3.81" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="3.81" x2="161.29" y2="3.81" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="387.35" columns="8" rows="5" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3P-LOC" urn="urn:adsk.eagle:component:13945/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>A3 Portrait Location</description>
<gates>
<gate name="G$1" symbol="A3P-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="Q1" library="dooba" deviceset="BSS138" device=""/>
<part name="R1" library="dooba" deviceset="RES" device="0805" value="10k"/>
<part name="R2" library="dooba" deviceset="RES" device="0805" value="10k"/>
<part name="Q2" library="dooba" deviceset="BSS138" device=""/>
<part name="R3" library="dooba" deviceset="RES" device="0805" value="10k"/>
<part name="R4" library="dooba" deviceset="RES" device="0805" value="10k"/>
<part name="Q3" library="dooba" deviceset="BSS138" device=""/>
<part name="R5" library="dooba" deviceset="RES" device="0805" value="10k"/>
<part name="R6" library="dooba" deviceset="RES" device="0805" value="10k"/>
<part name="Q4" library="dooba" deviceset="BSS138" device=""/>
<part name="R7" library="dooba" deviceset="RES" device="0805" value="10k"/>
<part name="R8" library="dooba" deviceset="RES" device="0805" value="10k"/>
<part name="P1" library="dooba" deviceset="PIN_6" device="1LINE"/>
<part name="P2" library="dooba" deviceset="PIN_6" device="1LINE"/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A3P-LOC" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="119.38" y="-40.64" size="3.81" layer="97">Dooba
Shifty schematic</text>
</plain>
<instances>
<instance part="Q1" gate="Q1" x="63.5" y="48.26" smashed="yes" rot="R270">
<attribute name="NAME" x="68.58" y="30.48" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="63.5" y="30.48" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R1" gate="R1" x="40.64" y="60.96" smashed="yes" rot="R270">
<attribute name="NAME" x="43.18" y="66.04" size="1.6764" layer="95" rot="R270"/>
<attribute name="VALUE" x="35.56" y="66.04" size="1.6764" layer="96" rot="R270"/>
</instance>
<instance part="R2" gate="R1" x="86.36" y="60.96" smashed="yes" rot="R270">
<attribute name="NAME" x="88.9" y="66.04" size="1.6764" layer="95" rot="R270"/>
<attribute name="VALUE" x="81.28" y="66.04" size="1.6764" layer="96" rot="R270"/>
</instance>
<instance part="Q2" gate="Q1" x="63.5" y="106.68" smashed="yes" rot="R270">
<attribute name="NAME" x="68.58" y="88.9" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="63.5" y="88.9" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R3" gate="R1" x="40.64" y="119.38" smashed="yes" rot="R270">
<attribute name="NAME" x="43.18" y="124.46" size="1.6764" layer="95" rot="R270"/>
<attribute name="VALUE" x="35.56" y="124.46" size="1.6764" layer="96" rot="R270"/>
</instance>
<instance part="R4" gate="R1" x="86.36" y="119.38" smashed="yes" rot="R270">
<attribute name="NAME" x="88.9" y="124.46" size="1.6764" layer="95" rot="R270"/>
<attribute name="VALUE" x="81.28" y="124.46" size="1.6764" layer="96" rot="R270"/>
</instance>
<instance part="Q3" gate="Q1" x="63.5" y="165.1" smashed="yes" rot="R270">
<attribute name="NAME" x="68.58" y="147.32" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="63.5" y="147.32" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R5" gate="R1" x="40.64" y="177.8" smashed="yes" rot="R270">
<attribute name="NAME" x="43.18" y="182.88" size="1.6764" layer="95" rot="R270"/>
<attribute name="VALUE" x="35.56" y="182.88" size="1.6764" layer="96" rot="R270"/>
</instance>
<instance part="R6" gate="R1" x="86.36" y="177.8" smashed="yes" rot="R270">
<attribute name="NAME" x="88.9" y="182.88" size="1.6764" layer="95" rot="R270"/>
<attribute name="VALUE" x="81.28" y="182.88" size="1.6764" layer="96" rot="R270"/>
</instance>
<instance part="Q4" gate="Q1" x="63.5" y="223.52" smashed="yes" rot="R270">
<attribute name="NAME" x="68.58" y="205.74" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="63.5" y="205.74" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R7" gate="R1" x="40.64" y="236.22" smashed="yes" rot="R270">
<attribute name="NAME" x="43.18" y="241.3" size="1.6764" layer="95" rot="R270"/>
<attribute name="VALUE" x="35.56" y="241.3" size="1.6764" layer="96" rot="R270"/>
</instance>
<instance part="R8" gate="R1" x="86.36" y="236.22" smashed="yes" rot="R270">
<attribute name="NAME" x="88.9" y="241.3" size="1.6764" layer="95" rot="R270"/>
<attribute name="VALUE" x="81.28" y="241.3" size="1.6764" layer="96" rot="R270"/>
</instance>
<instance part="P1" gate="P1" x="129.54" y="233.68" smashed="yes" rot="R180">
<attribute name="NAME" x="132.08" y="226.06" size="1.27" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="132.08" y="228.6" size="1.27" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="P2" gate="P1" x="129.54" y="203.2" smashed="yes" rot="R180">
<attribute name="NAME" x="132.08" y="195.58" size="1.27" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="132.08" y="198.12" size="1.27" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="GND1" gate="1" x="142.24" y="238.76" smashed="yes" rot="R90">
<attribute name="VALUE" x="144.78" y="236.22" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND2" gate="1" x="142.24" y="208.28" smashed="yes" rot="R90">
<attribute name="VALUE" x="144.78" y="205.74" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="FRAME1" gate="G$1" x="-48.26" y="-50.8" smashed="yes">
<attribute name="DRAWING_NAME" x="168.91" y="-35.56" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="168.91" y="-40.64" size="2.286" layer="94"/>
<attribute name="SHEET" x="182.245" y="-45.72" size="2.54" layer="94"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="LO_0" class="0">
<segment>
<pinref part="R7" gate="R1" pin="2"/>
<pinref part="Q4" gate="Q1" pin="S"/>
<wire x1="40.64" y1="226.06" x2="40.64" y2="220.98" width="0.1524" layer="91"/>
<wire x1="40.64" y1="220.98" x2="45.72" y2="220.98" width="0.1524" layer="91"/>
<wire x1="40.64" y1="220.98" x2="20.32" y2="220.98" width="0.1524" layer="91"/>
<junction x="40.64" y="220.98"/>
<label x="22.86" y="220.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P2" gate="P1" pin="6"/>
<wire x1="137.16" y1="215.9" x2="162.56" y2="215.9" width="0.1524" layer="91"/>
<label x="152.4" y="215.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="HI_0" class="0">
<segment>
<pinref part="Q4" gate="Q1" pin="D"/>
<pinref part="R8" gate="R1" pin="2"/>
<wire x1="81.28" y1="220.98" x2="86.36" y2="220.98" width="0.1524" layer="91"/>
<wire x1="86.36" y1="220.98" x2="86.36" y2="226.06" width="0.1524" layer="91"/>
<wire x1="86.36" y1="220.98" x2="106.68" y2="220.98" width="0.1524" layer="91"/>
<junction x="86.36" y="220.98"/>
<label x="99.06" y="220.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P1" gate="P1" pin="6"/>
<wire x1="137.16" y1="246.38" x2="162.56" y2="246.38" width="0.1524" layer="91"/>
<label x="152.4" y="246.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="LO" class="0">
<segment>
<pinref part="R1" gate="R1" pin="1"/>
<wire x1="40.64" y1="71.12" x2="40.64" y2="76.2" width="0.1524" layer="91"/>
<wire x1="40.64" y1="76.2" x2="63.5" y2="76.2" width="0.1524" layer="91"/>
<pinref part="Q1" gate="Q1" pin="G"/>
<wire x1="63.5" y1="76.2" x2="63.5" y2="66.04" width="0.1524" layer="91"/>
<wire x1="40.64" y1="76.2" x2="20.32" y2="76.2" width="0.1524" layer="91"/>
<junction x="40.64" y="76.2"/>
<label x="22.86" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R3" gate="R1" pin="1"/>
<wire x1="40.64" y1="129.54" x2="40.64" y2="134.62" width="0.1524" layer="91"/>
<wire x1="40.64" y1="134.62" x2="63.5" y2="134.62" width="0.1524" layer="91"/>
<pinref part="Q2" gate="Q1" pin="G"/>
<wire x1="63.5" y1="134.62" x2="63.5" y2="124.46" width="0.1524" layer="91"/>
<wire x1="40.64" y1="134.62" x2="20.32" y2="134.62" width="0.1524" layer="91"/>
<junction x="40.64" y="134.62"/>
<label x="22.86" y="134.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R5" gate="R1" pin="1"/>
<wire x1="40.64" y1="187.96" x2="40.64" y2="193.04" width="0.1524" layer="91"/>
<wire x1="40.64" y1="193.04" x2="63.5" y2="193.04" width="0.1524" layer="91"/>
<pinref part="Q3" gate="Q1" pin="G"/>
<wire x1="63.5" y1="193.04" x2="63.5" y2="182.88" width="0.1524" layer="91"/>
<wire x1="40.64" y1="193.04" x2="20.32" y2="193.04" width="0.1524" layer="91"/>
<junction x="40.64" y="193.04"/>
<label x="22.86" y="193.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R7" gate="R1" pin="1"/>
<wire x1="40.64" y1="246.38" x2="40.64" y2="251.46" width="0.1524" layer="91"/>
<wire x1="40.64" y1="251.46" x2="63.5" y2="251.46" width="0.1524" layer="91"/>
<pinref part="Q4" gate="Q1" pin="G"/>
<wire x1="63.5" y1="251.46" x2="63.5" y2="241.3" width="0.1524" layer="91"/>
<wire x1="40.64" y1="251.46" x2="20.32" y2="251.46" width="0.1524" layer="91"/>
<junction x="40.64" y="251.46"/>
<label x="22.86" y="251.46" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P2" gate="P1" pin="4"/>
<wire x1="137.16" y1="210.82" x2="162.56" y2="210.82" width="0.1524" layer="91"/>
<label x="152.4" y="210.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="HI" class="0">
<segment>
<pinref part="R2" gate="R1" pin="1"/>
<wire x1="86.36" y1="71.12" x2="86.36" y2="76.2" width="0.1524" layer="91"/>
<wire x1="86.36" y1="76.2" x2="106.68" y2="76.2" width="0.1524" layer="91"/>
<label x="99.06" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R4" gate="R1" pin="1"/>
<wire x1="86.36" y1="129.54" x2="86.36" y2="134.62" width="0.1524" layer="91"/>
<wire x1="86.36" y1="134.62" x2="106.68" y2="134.62" width="0.1524" layer="91"/>
<label x="99.06" y="134.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R6" gate="R1" pin="1"/>
<wire x1="86.36" y1="187.96" x2="86.36" y2="193.04" width="0.1524" layer="91"/>
<wire x1="86.36" y1="193.04" x2="106.68" y2="193.04" width="0.1524" layer="91"/>
<label x="99.06" y="193.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R8" gate="R1" pin="1"/>
<wire x1="86.36" y1="246.38" x2="86.36" y2="251.46" width="0.1524" layer="91"/>
<wire x1="86.36" y1="251.46" x2="106.68" y2="251.46" width="0.1524" layer="91"/>
<label x="99.06" y="251.46" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P1" gate="P1" pin="4"/>
<wire x1="137.16" y1="241.3" x2="162.56" y2="241.3" width="0.1524" layer="91"/>
<label x="152.4" y="241.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="LO_1" class="0">
<segment>
<pinref part="R5" gate="R1" pin="2"/>
<pinref part="Q3" gate="Q1" pin="S"/>
<wire x1="40.64" y1="167.64" x2="40.64" y2="162.56" width="0.1524" layer="91"/>
<wire x1="40.64" y1="162.56" x2="45.72" y2="162.56" width="0.1524" layer="91"/>
<wire x1="40.64" y1="162.56" x2="20.32" y2="162.56" width="0.1524" layer="91"/>
<junction x="40.64" y="162.56"/>
<label x="22.86" y="162.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P2" gate="P1" pin="5"/>
<wire x1="137.16" y1="213.36" x2="162.56" y2="213.36" width="0.1524" layer="91"/>
<label x="152.4" y="213.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="HI_1" class="0">
<segment>
<pinref part="Q3" gate="Q1" pin="D"/>
<pinref part="R6" gate="R1" pin="2"/>
<wire x1="81.28" y1="162.56" x2="86.36" y2="162.56" width="0.1524" layer="91"/>
<wire x1="86.36" y1="162.56" x2="86.36" y2="167.64" width="0.1524" layer="91"/>
<wire x1="86.36" y1="162.56" x2="106.68" y2="162.56" width="0.1524" layer="91"/>
<junction x="86.36" y="162.56"/>
<label x="99.06" y="162.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P1" gate="P1" pin="5"/>
<wire x1="137.16" y1="243.84" x2="162.56" y2="243.84" width="0.1524" layer="91"/>
<label x="152.4" y="243.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="LO_2" class="0">
<segment>
<pinref part="R3" gate="R1" pin="2"/>
<pinref part="Q2" gate="Q1" pin="S"/>
<wire x1="40.64" y1="109.22" x2="40.64" y2="104.14" width="0.1524" layer="91"/>
<wire x1="40.64" y1="104.14" x2="45.72" y2="104.14" width="0.1524" layer="91"/>
<wire x1="40.64" y1="104.14" x2="20.32" y2="104.14" width="0.1524" layer="91"/>
<junction x="40.64" y="104.14"/>
<label x="22.86" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P2" gate="P1" pin="2"/>
<wire x1="137.16" y1="205.74" x2="162.56" y2="205.74" width="0.1524" layer="91"/>
<label x="152.4" y="205.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="HI_2" class="0">
<segment>
<pinref part="Q2" gate="Q1" pin="D"/>
<pinref part="R4" gate="R1" pin="2"/>
<wire x1="81.28" y1="104.14" x2="86.36" y2="104.14" width="0.1524" layer="91"/>
<wire x1="86.36" y1="104.14" x2="86.36" y2="109.22" width="0.1524" layer="91"/>
<wire x1="86.36" y1="104.14" x2="106.68" y2="104.14" width="0.1524" layer="91"/>
<junction x="86.36" y="104.14"/>
<label x="99.06" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P1" gate="P1" pin="2"/>
<wire x1="137.16" y1="236.22" x2="162.56" y2="236.22" width="0.1524" layer="91"/>
<label x="152.4" y="236.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="LO_3" class="0">
<segment>
<pinref part="R1" gate="R1" pin="2"/>
<pinref part="Q1" gate="Q1" pin="S"/>
<wire x1="40.64" y1="50.8" x2="40.64" y2="45.72" width="0.1524" layer="91"/>
<wire x1="40.64" y1="45.72" x2="45.72" y2="45.72" width="0.1524" layer="91"/>
<wire x1="40.64" y1="45.72" x2="20.32" y2="45.72" width="0.1524" layer="91"/>
<junction x="40.64" y="45.72"/>
<label x="22.86" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P2" gate="P1" pin="1"/>
<wire x1="137.16" y1="203.2" x2="162.56" y2="203.2" width="0.1524" layer="91"/>
<label x="152.4" y="203.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="HI_3" class="0">
<segment>
<pinref part="Q1" gate="Q1" pin="D"/>
<pinref part="R2" gate="R1" pin="2"/>
<wire x1="81.28" y1="45.72" x2="86.36" y2="45.72" width="0.1524" layer="91"/>
<wire x1="86.36" y1="45.72" x2="86.36" y2="50.8" width="0.1524" layer="91"/>
<wire x1="86.36" y1="45.72" x2="106.68" y2="45.72" width="0.1524" layer="91"/>
<junction x="86.36" y="45.72"/>
<label x="99.06" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P1" gate="P1" pin="1"/>
<wire x1="137.16" y1="233.68" x2="162.56" y2="233.68" width="0.1524" layer="91"/>
<label x="152.4" y="233.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="P1" gate="P1" pin="3"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="139.7" y1="238.76" x2="137.16" y2="238.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P2" gate="P1" pin="3"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="139.7" y1="208.28" x2="137.16" y2="208.28" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
